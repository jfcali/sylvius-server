var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

const axios 		= require('axios');
const host = "http://192.168.100.3";
//const host = "http://indissoluble.talkingbrains.api.john.cat";

const phidgets 		= require('phidgets');


var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// socket.io server para enviar eventos a los client
app.io        = require('socket.io')(4000);
app.ioClient  = require('socket.io-client');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var localSessionStarted = false;
var ik;

var rfid;

var currentStage = 'test';
var userID;

app.io.on('connection', function(socket) {
	console.log('connection');

	socket.on('disconnect', function() {
		console.log('disconnect', userID);
		localSessionStarted = false;
		if(userID) {
			axios.post(host + '/deactivation', {"token":"eb856bc68a4622b9b2f0bb7d500faf7ea96ea804", "id": userID, "module":"sylvius", "station":currentStage})
			.then(function (response) {
				console.log("user disconnect ok",response.data.user, response.status, response.statusText);
				
				//app.io.emit('localSessionStart', { user: response.data.user });
				
				localSessionStarted = false;

			})
			.catch(function (error) {
				console.log("error deactivation: sending user with no data", error);
				//app.iosEmitter.sendEvent("localSessionStart",{"type":"rfid"});
				
				//app.io.emit('localSessionStart', {  user: response.data.user });
				
				localSessionStarted = false;
			});
		}
	});
	socket.emit('connected', { hello: 'world' });
	socket.on('my other event', function (data) {
		console.log(data);
	});
	socket.on('userTouch', function (data) {
		localSessionStarted = true;
	});
});

ik = new phidgets.PhidgetRFID()
	.on('opened', function(emitter, data) {
		emitter.antenna = true;
		console.log("PhidgetRFID opened, antenna ready");
	})
	.on('error', function(emitter, data) {
		console.log("PhidgetRFID ERROR. Check the phidgets drives are installed and that the webservice is on");
		process.stderr.write("\007");
	})
	.on('detected', function(emitter, tag) {
		//if (!state.active) {
			rfid = tag.value;
			currentStage = '1';
			// const rfid = "0087f3bc95";
			if(localSessionStarted) {
				//app.io.emit('userAction', {rfid: rfid});
				console.log("/activation has been activated already", {rfid: rfid});
			} else {
				console.log("/activation","{rfid:",rfid,"module:sylvius, station:"+currentStage+"}");
					
				//app.io.emit('localSessionStart', { index: index });
				console.log('llamada')
				localSessionStarted = true;
				axios.post(host + '/activation', {
					"token":"eb856bc68a4622b9b2f0bb7d500faf7ea96ea804", 
					"rfid": rfid, 
					"module":"sylvius", 
					"station":currentStage
				})
				.then(function (response) {
					console.log("user get ok",response.data.user, response.status, response.statusText);
					
					app.io.emit('localSessionStart', { user: response.data.user });
					userID = response.data.user._id;
					
					localSessionStarted = true;
					

				})
				.catch(function (error) {
					console.log("error post: sending user with no data", error);
					//app.iosEmitter.sendEvent("localSessionStart",{"type":"rfid"});
					
					app.io.emit('localSessionStart', { hello: error });
					
					localSessionStarted = true;
				});
			}
		//}
	})
	.on('timeout', function(){
		console.log("Error: PhidgetRFID can not be found");
		process.stderr.write("\007");
	})
	.open();
module.exports = app;
